# Another Super Duper Script Editor - ASDSE

Fast and efficient script editor for Danganronpa Games

**Requirements :**

- python 3.5.4
    - https://www.python.org/downloads/release/python-354/
    
- pyqt5 : type this line below in a terminal
    - `pip install pyqt5`
    
- selenium : type this line below in a terminal
    - `pip install selenium`

